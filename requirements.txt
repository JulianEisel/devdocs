markdown-callouts>=0.3.0
mkdocs-material>=9.4.5
mkdocs-glightbox>=0.3.4
mkdocs-git-revision-date-localized-plugin>=1.2.0
mkdocs-section-index>=0.3.8
pygments>=2.16.1
pymdown-extensions>=10.3

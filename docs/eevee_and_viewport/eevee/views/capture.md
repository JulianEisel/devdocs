# CaptureView

A view to capture reflection probes from the scene.

```mermaid
classDiagram
    class CaptureView {
        render_world()
        render_probes()
    }
```

The CaptureView asks the `ReflectionProbeModule` if any reflection probe
needs to be rerendered. If that is the case it will be rendered and
or updated.

See [ReflectionProbeModule](/eevee_and_viewport/eevee/modules/reflection_probes/) for detailed
information.
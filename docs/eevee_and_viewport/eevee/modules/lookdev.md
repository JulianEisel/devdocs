# Lookdev Module

The lookdev modules is responsible for overriding the world when a custom HDRI should be used and to render look development spheres in the corner of the screen.

## Custom HDRI

In the 3d viewport the user can choose to show the world of the scene, or override the world with a custom HDRI.

## Look Development Spheres

> TODO: Not implemented yet.
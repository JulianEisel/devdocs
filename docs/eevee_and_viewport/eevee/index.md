# Eevee

Eevee is a draw/render engine that uses game engine like techniques to
provide a fast render engine.

## Design principles

### Drop-in replacement for Cycles

Eevee should be a drop in replacement for Cycles. Eevee is used to draw the material preview mode for Cycles. Therefore the shading is tweaked to ensure that it follows Cycles as close as possible, but still be fast in rendering.



## Modules

- [World](modules/world.md)
- [Lookdev](modules/lookdev.md)
- [Reflection Probes](modules/reflection_probes.md)

## Views

- [Capture](views/capture.md)
## Pipelines

- [Background](pipelines/background.md)
- [World](pipelines/world.md)
- [DeferredProbePipeline](pipelines/probe.md)

## Shaders

